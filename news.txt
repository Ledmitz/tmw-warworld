##1Thank you for trying WarWorld updates.##0
##9You should experience more sounds and music.##0
##9There are some tweaks to particles also.##0
##9If you get errors, uncheck custom updates host##0
##9to revert back to default TMW updates.##0

Visit http://warworld.noads.biz for info.
TMW News can be found by visiting:
https://www.themanaworld.org/
Or:
https://gitlab.com/Ledmitz/tmw-warworld/-/blob/master/news.txt



##92022-06-28##0

- Improved newer additions and increased the speed of
animations on my party badge
- Added new, more direct particle effect for confringo by Relm

##1---------------------------------------------------------##0

##92022-06-24##0

- Improved Inv sprite for The Book of Floyd and other fun

##1---------------------------------------------------------##0

##92022-06-17##0

- Added The Book of Floyd

##1---------------------------------------------------------##0

##92022-05-07##0

- Added SFX for Reaper, Ball Lightning, Scythe and a little
surprise for Livio.... for now.  XD
EDIT: Adjusted volumes

##1---------------------------------------------------------##0

##92022-05-02##0

- Added shields (because I wanted to beat the server to it...
 hehe) Thank you HoraK.

##1---------------------------------------------------------##0

##92022-04-30##0

- Event update - CRC Day

##1---------------------------------------------------------##0



##92022-04-26##0

- Server update
- Added new player punch sounds, upmarmu incl.
- Added dagger sprite to Fallen since I had a knife sound for
it. I could have sworn they had daggers before and they do
high damage so it seems justified.

##1---------------------------------------------------------##0

##92022-03-17##0

- Server update
- Added fire/smoke effect to Lava Slime

##1---------------------------------------------------------##0

##92022-02-28##0

- This is a temp fix resolving screen freeze from missing
frames in Terogan's reference to evileyes.xml

##1---------------------------------------------------------##0

##92022-02-27##0

- Server update (groups.xml, Flashmob and Pumkin added)

##1---------------------------------------------------------##0

##92021-12-27##0

- Just getting back up to date with the server.
- Carrot corrected (later edit)
MERRY CHRISTMAS, HAPPY HOLIDAYS and HAPPY NEW YEAR!

##1---------------------------------------------------------##0

##92021-03-17##0

- Reduced size and lowered vol of flame.ogg, minor tweaks and
server update. 

##1---------------------------------------------------------##0

##92021-03-09##0

- Replaced missing music. Thank you, PiNG, for mentioning.

##1---------------------------------------------------------##0

##92021-03-06##0

- Multiple placeholders added as temporary sounds
- All slimes have more sounds
- Added 2 Final Showdown music tracks to Illia quest
- Skulls and lava slime have extra particle effects on crits
Some SFX are reused. I feel some are better than none, in most
cases.

##1---------------------------------------------------------##0

##92021-02-11##0

- Beheader added

##1---------------------------------------------------------##0

##92020-12-18##0

- Confringo finally altered to minimize particle as requested.
- Music wasn't updated last time as intended. Fixed now.
- Server restored original Xmas tree hat. 

##1---------------------------------------------------------##0

##92020-12-18##0

- All sfx, including server, have had volume tweaks and have been
resampled and most converted to mono.
- All music over has been crunched and many have loop edits now.
- Clort's additions now include knife and sword misses, short
sword hits and really great pollett SFX.
- Forgot to mention I patched graveyard sprite sheets with
Micksha's versions already.
- Nutcracker's original sprite sheet restored which gives custom
animations and alleviates lag from non-existent player actions.
- Probably some other tweaks I forgot to mention and some
renaming of files.

##1---------------------------------------------------------##0

##92020-12-01##0

Happy to add short sword miss sounds by clort and more to come
from him. Welcome clort. :-)... + Xmas server update.

##1---------------------------------------------------------##0

##92020-11-24##0

Readded Dark Talisman custom effect, but now only appears at
feet as a twitchy shadow. 

##1---------------------------------------------------------##0

##92020-11-12##0

Removed Dark Talisman custom effect to allow use of new server
version by Hello=). Otherwise, just a server update. 

##1---------------------------------------------------------##0

##92020-10-30##0

Corrected ferry bell error and added particle for Dark
Talisman. 

##1---------------------------------------------------------##0

##92020-10-24##0

Sorry for this update. Re-added missing SFX from effects.xml
after conforming to Eathena syntax. Tengu volume raised
slightly. 

##1---------------------------------------------------------##0

##92020-10-19##0

Just bug fixes to eliminate lag from non-existent sounds.

##1---------------------------------------------------------##0

##92020-09-21##0

Some sounds were tweaked in volume for Legacy requests.
Added sounds for Tengu and changed Mana Guardian attack SFX
from chiza to confringo as there is no knife sprite to match.

##1---------------------------------------------------------##0

##92020-08-24##0

Added Golem hit and miss. Changed bow sprite of enchanter and
koyntety to Forest Bow and added matching sfx. Fixed floating
Golem issue. Lowered Rock Knife volume and other small tweaks.

##1---------------------------------------------------------##0

##92020-04-29##0

Added Ghosts, Fallen, Sasquatch, FlashMob, Mana Guardian and
Mana Slayer. Added Halberd hits (for FlashMob)
Remade ingrav and some volume tweaks.
Reworked Setzer. Selected best 3, lowered pitch and compressed.
Forgot to mention: Added bees, sword-miss, added particle
effects for electric.yellow/.persist, magic.black/.persist,
magic.green/.persist and tweaked all ingrav particles to have
extra function and more variation. Customized particle for
confringo.
Changed in town music of Hurnscald to ambience of birds
singing and made indoors quiet in many places.

##1---------------------------------------------------------##0

##92020-04-24##0

A few minor bug fixes and tweaks. Removed upmarmu crit and
gloves and replaced with magic hit particle. Stopped patching
_include.xml files as it was unnecessary and caused errors on
TMW updates. Updates may come more frequently soon.

##1---------------------------------------------------------##0

##92019-01-26##0

Reworked Skeleton, Lady Skeleton, Jacko and Terranite.
Added Yeti sounds.

##1---------------------------------------------------------##0

##92018-11-14##0

Added confringo, life, chiza, flar, miteyo, discharge, frillyar,
shrooms sounds by Davius, snail sounds by alpha, spider reworked.
Includes butterfly hit sounds by alpha. Spider moves and die
sound author is unknown currently. Fixed missing Jacko and
scythe sounds.

New music: { Barbarians, candr2, chilling environment(TMW
submissions - authors unknown ATT) } and I made Sewer Ambience 
for Tulimshar sewers. Clouds Calling edited and brought back.

Created a dart animation to use with flar. Hoping to add a
particle layer over top of it so that dart can be used as a
regular weapon too.

Some other particle tweaks.

##1---------------------------------------------------------##0

Bonus sounds: rain, astral, betsanc, flame, generic, ingrav,
nature, transmute, war, jacko, lady/skeleton, terranite,
boom, critical, alt level up, alt skill up, warp,
reg/short/forest/banshee bow, reg/sharp/dagger/bone/rock/setzer
knives, scythe, staff, misses and a punch.
Removed Grenadier bell sound.

Bonus music: Crypt - Overwhelmed Underground, Swamp - Dreary
Day, Candor (cave) - Genesis and one goofy easter egg.

##1---------------------------------------------------------##0
