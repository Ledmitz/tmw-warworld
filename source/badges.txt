graphics/badges/parties/freelang.png
https://code.google.com/p/hudson-assembler/downloads/detail?name=griffon-icon-16x16.png&can=2&q=
code under Apache 2, in other sources CDDL. Icons probably same.

graphics/badges/parties/quatrefleurs.png
http://www.iconsplace.com/262030-icons/darth-vader-icon
License: This icon is provided by Jory Raphael as Creative Commons Attribution 3.0 Unported

graphics/badges/parties/dune.png
http://www.iconsplace.com/262030-icons/death-star-icon
License: This icon is provided by Jory Raphael as Creative Commons Attribution 3.0 Unported

graphics/badges/parties/king'scloud.png
http://www.iconsplace.com/maroon-icons/jango-fett-icon
License: This icon is provided by Jory Raphael as Creative Commons Attribution 3.0 Unported

graphics/badges/parties/petpeeve.png
http://www.iconsplace.com/maroon-icons/death-star-icon
License: This icon is provided by Jory Raphael as Creative Commons Attribution 3.0 Unported

graphics/badges/parties/ronin.png
http://www.iconsplace.com/lime-icons/darth-vader-icon
License: This icon is provided by Jory Raphael as Creative Commons Attribution 3.0 Unported

graphics/badges/parties/horizon.png
http://www.iconsplace.com/maroon-icons/chewbacca-icon
License: This icon is provided by Jory Raphael as Creative Commons Attribution 3.0 Unported

graphics/badges/parties/nightingale.png
http://trisquel.info/en/wiki/logo
License: GPL

graphics/badges/parties/jardind'enfants.png
http://www.iconsplace.com/maroon-icons/r2-d2-icon
License: This icon is provided by Jory Raphael as Creative Commons Attribution 3.0 Unported

graphics/badges/parties/quietsouls.png
http://www.iconsplace.com/maroon-icons/c-3po-icon
License: This icon is provided by Jory Raphael as Creative Commons Attribution 3.0 Unported

graphics/badges/guilds/diablurav.png
License: CC-BY-SA by Tirifto

graphics/badges/parties/celestiasteaparty.png
License: CC0 1.0 (mekolat)

graphics/badges/parties/theavenged.png
Source: https://commons.wikimedia.org/wiki/File:Bouclier_Captain_America_1018.png
Licence: public domain.

graphics/badges/parties/theshield.png
License: CC0 (alexander1006)

graphics/badges/parties/anaksd.png
License: CC-By v4.0 (Arkhen)

graphics/badges/parties/moneyway.png
Source: https://commons.wikimedia.org/wiki/File:Money.png
License: CC-By (Mark James)

graphics/badges/guilds/skyfall.png
Source: https://forums.themanaworld.org/viewtopic.php?p=149352#p149352
License: CC-BY-SA 3 (wushin)

graphics/badges/parties/no-name-worms.png
Source: Evol online playerset heads.
License: CC BY-SA 3.0 (Author: Reid, Contributors: Modanung, Talaroc, nelson6e65)

graphics/badges/parties/saulcarmy.png
Source: https://commons.wikimedia.org/wiki/File:CrossSword.png
License: CC BY 3.0 (FatCow Web Hosting, MAF-Saulc-Youtube)

graphics/badges/parties/guardiansoftulimshar.png
graphics/badges/parties/darkmages.png
Source: The mana world client data repository
License: GPL 2

graphics/badges/parties/pool.png
Source: http://findicons.com/icon/163964/sport_8ball
License: CC BY (FatCow Web Hosting)

graphics/badges/parties/death.png
Source: The mana world client data repository
License: GPL 2

graphics/badges/parties/thearcherscreed.png
Source: https://forums.themanaworld.org/viewtopic.php?p=150308#p150308
License: GPL 2 (Nami)

graphics/badges/parties/lamafia.png
Source: https://forums.themanaworld.org/viewtopic.php?p=150419#p150419
License: GPL 2 (Xtreem)

graphics/badges/parties/thebugsofsystem.png
Source: https://commons.wikimedia.org/wiki/File:Crystal_Project_bug.png
License: CC-BY-SA (???, jpfox)

graphics/badges/guilds/bodilyfluids.png
Source: https://www.iconfinder.com/icons/10173/water_icon#size=16
License: CC-BY 2.5 (Ben Gillbanks)

graphics/badges/parties/higherlevel.png
Source: https://forums.themanaworld.org/viewtopic.php?p=151598#p151598
License: CC-BY-SA 4.0 (Ledmitz)

graphics/guilds/moussaieff.png
Source: https://www.themanaworld.org/index.php/File:Generic-redsummonflower.png
License: CC-BY-SA 3.0 (Korcha, V0id, Enchilado

graphics/guilds/phoenixcouncil.png
Source: https://commons.wikimedia.org/wiki/File:FPWorldMap_Symbol_Bird.png
License: CC-BY-SA 3.0 (JovanCormac, Narus)

graphics/parties/theflamingphoenix.png
Source: http://findicons.com/icon/117411/fire_big?id=120905
License: CC-BY-SA (Yusuke Kamiyamane)

graphics/parties/cookieempire.png
Source: https://forums.themanaworld.org/viewtopic.php?p=152014#p152014
License: CC-BY-SA (cookie monster)

graphics/parties/sharpshutterarchers.png
Source: https://forums.themanaworld.org/viewtopic.php?p=152034#p152034
License: CC-BY-SA (romme)

graphics/parties/followthewhiterabbit.png
Source: https://forums.themanaworld.org/viewtopic.php?p=152078#p152078
Licence: CC-BY-SA (Micksha, Omatt)

graphics/guilds/crc.png
Source: http://crc.evolonline.org/news_entries/343473
License: CC-BY-SA (Micksha)
