## Music License

The information in this file might be incomplete or incorrect,
since the artists weren't tracked during the first years of the project.
If you have knowledge about some information that's missing or that
needs a correction, please contact the The Mana World development team.

**GPLv2**: Licensed under [GNU General Public License version 2](
http://www.gnu.org/licenses/gpl-2.0.html).

**CC BY-SA 3.0**: Licensed under [Creative Commons Attribution-ShareAlike 3.0 Unported](
http://creativecommons.org/licenses/by-sa/3.0/).

(:grey_question:): Unknown contributors.


 Title / File | Composers / Artists | Licenses | Date Created
 ------------ |:-------------------:|:-------:|:-----------:
"Mystique Forest" / `mystique-forest.ogg`  | Jose. S & Dakota. D (Chicka-Maria)        | GPLv2, cc-sa-3           | 2013
`peace.ogg`                                | Doulos                                    | GPLv2,                   | 2004
`snow-village.ogg`                         | Aasif Chaudry (Dariunas)                  | GPLv2, cc-sa-3           | 2008
`despair.ogg`                              | Jose. S & Dakota. D (Chicka-Maria)        | GPLv2, cc-sa-3           | (:grey_question:)
`explorers-melody.ogg`                     | Jose. S & Dakota. D (Chicka-Maria)        | GPLv2, cc-sa-3           | (:grey_question:)
`faith.ogg`                                | (:grey_question:)                         | GPLv2,                   | (:grey_question:)
`graveyard.ogg`                            | Feline Monstrosity                        | GPLv2,                   | (:grey_question:)
`cavesong.ogg`                             | finalman & yakslem                        | GPLv2,                   | (:grey_question:)
`inquisitive-inn.ogg`                      | Dakota. D (Chicka-Maria)                  | GPLv2, cc-sa-3           | (:grey_question:)
`magick-real.ogg`                          | Aasif Chaudry (Dariunas)                  | GPLv2, cc-sa-3           | 2003
`night-is-calling.ogg`                     | Jose. S & Dakota. D (Chicka-Maria)        | GPLv2, cc-sa-3           | (:grey_question:)
`the-forest.ogg`                           | Aasif Chaudry  (Dariunas)                 | GPLv2,                   | (:grey_question:)
`white-christmas.ogg`                      | Jose S. & Dakota. D (Chicka-Maria)        | GPLv2, cc-sa-3           | (:grey_question:)
`dimonds-cove.ogg`                         | Aasif Chaudry  (Dariunas)                 | GPLv2, cc-sa-3           | 2008
`sail-away.ogg`                            | Jose. S & Dakota. D (Chicka-Maria)        | GPLv2, cc-sa-3           | 2014-04-23
`Dramatic.ogg`                             | Sei Mutsuki (PeriTune/peritune.com)       | cc-by-4                  | 2019
`ominous3.ogg`                             | Sei Mutsuki (PeriTune/peritune.com)       | cc-by-4                  | (:grey_question:)
`ghoulish-fun.ogg`                         | Eric Matyas, Riqqi/Evol Online            | cc-by-4                  | 2019
`epilogue.ogg`                             | Jose. S & Dakota. D (Chicka-Maria)        | GPLv2, cc-sa-3           | 2020
`Barbarians-fixed.ogg`                     | Dakota. D (Chicka-Maria) (Ledmitz edit)   | GPLv2, cc-sa-3           | 2012
`Candr2.ogg`                               | Elite                                     | GPLv2, cc-sa-3           | (:grey_question:)
`Chilling_Environment-fixed.ogg`           | Dakota. D (Yubaba) (edited by Ledmitz)    | GPLv2, cc-sa-3           | 2011
`Clouds_Calling-edit.ogg`                  | (:grey_question:) (edited by Ledmitz)     | GPLv2,                   | (:grey_question:)
`DrearyDay.ogg`                            | Ledmitz                                   | cc-sa-3                  | 2015
`Genesis.ogg`                              | Ledmitz                                   | cc-sa-3                  | 2016
`minigame-01.ogg`                          | Ledmitz                                   | cc-sa-3                  | 2016
`OverwhelmedUnderground.ogg`               | Ledmitz                                   | cc-sa-3                  | 2015
`SewerAmbience.ogg`                        | Ledmitz                                   | cc-sa-3                  | 2018
`Hurnscald.ogg`                            | Ledmitz                                   | cc-sa-3                  | 2020
